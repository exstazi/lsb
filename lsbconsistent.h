#ifndef LSBCONSISTENT_H
#define LSBCONSISTENT_H

#include "lsbblock.h"
#include "bmpreader.h"

class LsbConsistent: public LsbBlock
{
public:
    LsbConsistent( BmpReader *reader_image,int c_part,int s_block );
    void writeMessage(QString message);
    QString readMessage(int count);

    void writeInfoOnFile(QString message);
    QString readInfoOnFile(int count_block);

protected:
    QVector<QPair<int,int> > getPoint(int count_block);
};

#endif // LSBCONSISTENT_H
