#include "lsbrandom.h"

LsbRandom::LsbRandom(BmpReader *reader_image,int c_part,int s_block)
{
    this->image = reader_image;

    count_bits = 1;
    is_need_red_component =
            is_need_green_component =
                    is_need_blue_component = true;
    summa_component = 3;

    count_part = c_part;
    size_block = s_block;

    w_part = image->getWidth() / count_part;
    h_part = image->getHeight() / count_part;
}

/*
 * Запись сообщения в файл
 * message - встриваемое сообщение
 */
void LsbRandom::writeMessage(QString message)
{
    QBitArray arr( stringToBitArray( message ) );

    writeToFile( getPoint( getDesiredCountBlock( arr.size() ) ), arr );
}

/*
 * Запись информации о файле
 * message - информаця о файле
 */
void LsbRandom::writeInfoOnFile(QString message)
{
    //Добавление информаци о номере блока
    message.append( "Block 0" );

    //Если длина сообщения превышает максимальную длину, то обрезаем сообщениеы
    if( message.size() > getMaxLengthMessage() )
    {
        message.resize( getMaxLengthMessage() );
    }
    //Преобразовываем сообщение в массив бит
    QBitArray arr( stringToBitArray(message) );

    //Встраиваем в первые 2 пикселя каждого блока длину сообщения
    writeCountSymbolsForInfoOnFile( message.size() );

    QVector<QPair<int,int> > position;
    //Получаем размеры одного блока
    int w_border = image->getWidth() / 2;
    int h_border = (image->getHeight() / 2) - 1;
    //Получаем небходимое число блоков для встраивания
    int count_block = getDesiredCountBlock( arr.size() );

    //Инициализируем генератор всевдослучайных числе
    RepeatedSequence::setParam( w_border,h_border,count_part*count_part );

    for( int n = 0; n < 4; ++n )
    {
        RepeatedSequence::setSeed( message.size() );
        //Заменяем номер блока в сообщения на текущий счетчик
        arr = stringToBitArray( message.left( message.size()-1) + QString::number(n+1) );
        for( int i = 0; i < count_block; ++i )
        {
            //Получаем координаты пикселей для выбранного блока
            position += getCoordinate( RepeatedSequence::rand(),w_border/count_part,h_border/count_part, n /2 * w_border,n%2*h_border );
        }
        //Записываем инфомрацию в файл
        writeToFile( position, arr );
        position.clear();
    }
}

/*
 * Считывание информации о файле
 * count_block - номер блока для считывания
 */
QString LsbRandom::readInfoOnFile(int count_block)
{
    //Получаем размеры одного блока
    int w_border = image->getWidth() / 2;
    int h_border = ( image->getHeight() / 2) - 1;

    //Считываем из первых двух символов размер встроенной информации
    int length_info = readCountSymbolsForInfoOnFile( count_block );

    //Рассчитываем необходимое колиечстов блоков, для считывания
    int need_block = getDesiredCountBlock( length_info * 8 );

    QVector<QPair<int, int> > position;
    //Инициализируем генератор псевдослучайных чисел
    RepeatedSequence::setSeed( length_info );
    RepeatedSequence::setParam( w_border,h_border,count_part*count_part );

    for( int i = 0; i < need_block; ++i )
    {
        //Получаем координаты пикселей для выбранного блока
        position += getCoordinate( RepeatedSequence::rand(),w_border/count_part,h_border/count_part,count_block /2 * w_border,count_block%2*h_border  );
    }

    return readFile( position,length_info );
}

/*
 * Считывание сообщения из файла
 * count - количество считываемых символов
 */
QString LsbRandom::readMessage( int count )
{
    return readFile( getPoint( getDesiredCountBlock( count * 8 )),count );
}

/*
 * Создание массива пикселей для выбранного колиечства блоков
 * count_block - колиечство необходимых блоков
 */
QVector<QPair<int, int> > LsbRandom::getPoint( int count_block )
{
    //Инициализация генератора псевдослучайных чисел
    RepeatedSequence::setSeed( count_block / 8 );
    RepeatedSequence::setParam( image->getWidth(),image->getHeight(),count_part * count_part );

    QVector<QPair<int,int> > position;

    //Перебираем все блоки
    for( int i = 0; i < count_block; ++i )
    {
        //Получаем координаты пикслей для выбранного блока
        position += getCoordinate( RepeatedSequence::rand(),w_part,h_part );
    }

    return position;
}
