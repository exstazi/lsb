#ifndef REPEATEDSEQUENCE_H
#define REPEATEDSEQUENCE_H

#include <QObject>
#include <QList>

class RepeatedSequence
{
private:
    static int A;
    static int B;
    static int M;
    static long seed;
    static QList<int> set;
public:
    static int rand();
    static void setSeed( int s );
    static void setParam(int a, int b , int m);
};

#endif // REPEATEDSEQUENCE_H
