#include "lsbconsistent.h"

LsbConsistent::LsbConsistent(BmpReader *reader_image, int c_part, int s_block)
{
    this->image = reader_image;

    count_bits = 1;
    is_need_red_component =
            is_need_green_component =
                    is_need_blue_component = true;
    summa_component = 3;

    count_part = c_part;
    size_block = s_block;

    w_part = image->getWidth() / count_part;
    h_part = image->getHeight() / count_part;
}

/*
 * Запись сообщения в файл
 * message - встраиваемое сообщение
 */
void LsbConsistent::writeMessage(QString message)
{
    QBitArray arr( stringToBitArray( message ) );

    writeToFile( getPoint( getDesiredCountBlock( arr.size() ) ), arr );
}

/*
 * Встраивание информации о файле
 * message - информация о файле
 */
void LsbConsistent::writeInfoOnFile(QString message)
{
    //Добавление информаци о номере блока
    message.append( "Block 0" );
    //Если длина сообщения превышает максимальную длину, то обрезаем сообщениеы
    if( message.size() > getMaxLengthMessage() )
    {
        message.resize( getMaxLengthMessage() );
    }
     //Преобразовываем сообщение в массив бит
    QBitArray arr( stringToBitArray(message) );
    //Встраиваем в первые 2 пикселя каждого блока длину сообщения
    writeCountSymbolsForInfoOnFile( message.size() );

    QVector<QPair<int,int> > position;
    //Получаем размеры одного блока
    int w_border = image->getWidth() / 2;
    int h_border = (image->getHeight() / 2) - 1;
    //Получаем небходимое число блоков для встраивания
    int count_block = getDesiredCountBlock( arr.size() );

    for( int n = 0; n < 4; ++n )
    {
        //Заменяем номер блока в сообщения на текущий счетчик
        arr = stringToBitArray( message.left( message.size()-1) + QString::number(n+1) );
        for( int i = 0; i < count_block; ++i )
        {
            //Получаем координаты пикселей для i блока
            position += getCoordinate( i,w_border/count_part,h_border/count_part, n /2 * w_border,n%2*h_border );
        }
        //Записываем инфомрацию в файл
        writeToFile( position, arr );
        position.clear();
    }
}

/*
 * Считывание информации о сообщения
 * count_block - номер блока из которого производится считывание
 */
QString LsbConsistent::readInfoOnFile(int count_block)
{
    //Рассчет размеров каждого блока
    int w_border = image->getWidth() / 2;
    int h_border = ( image->getHeight() / 2) - 1;
    //считывание количества символов в информции
    int length_info = readCountSymbolsForInfoOnFile( count_block );
    //Рассчет необходимого колиечства блоков
    int need_block = getDesiredCountBlock( length_info * 8 );

    QVector<QPair<int, int> > position;

    for( int i = 0; i < need_block; ++i )
    {
        //Получение координат пикселей для i блока
        position += getCoordinate( i,w_border/count_part,h_border/count_part,count_block /2 * w_border,count_block%2*h_border  );
    }

    return readFile( position,length_info );
}

/*
 * Считывание сообщения из изображения
 * count - количество символов, которые необходимо считать
 */
QString LsbConsistent::readMessage(int count)
{
    return readFile( getPoint( getDesiredCountBlock( count * 8 ) ), count );
}

/*
 * Рассчет координат пикселей для выбранного количества блоков
 * count_block - количество блоков
 */
QVector<QPair<int, int> > LsbConsistent::getPoint( int count_block )
{
    QVector<QPair<int,int> > position;

    for( int i = 0; i < count_block; ++i )
    {
        position += getCoordinate( i,w_part,h_part );
    }
    return position;
}

