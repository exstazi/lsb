#ifndef LSBBLOCK_H
#define LSBBLOCK_H

#include "steganography.h"

class LsbBlock: public Steganography
{
protected:
    int count_part;
    int size_block;

    int w_part;
    int h_part;

public:
    void setCountPart( int value );
    void setSizeBlock( int value );

    int getMaxLengthMessage();

protected:
    QVector<QPair<int,int> > getCoordinate(int number_block , int w_part, int h_part, int start_offset_w = 0, int start_offset_h = 0);

    virtual QVector<QPair<int,int> > getPoint(int count_block) = 0;

    int getDesiredCountBlock( int count );
};

#endif // LSBBLOCK_H
