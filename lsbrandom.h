#ifndef LSBRANDOM_H
#define LSBRANDOM_H

#include "lsbblock.h"
#include "bmpreader.h"
#include "repeatedsequence.h"

class LsbRandom: public LsbBlock
{

public:
    LsbRandom(BmpReader *reader_image , int c_part, int s_block);

    void writeMessage(QString message);
    QString readMessage(int count);

    void writeInfoOnFile(QString message);
    QString readInfoOnFile(int count_block);
protected:
    QVector<QPair<int,int> > getPoint( int count_block );
};

#endif
