#ifndef BMPREADER_H
#define BMPREADER_H

#include <QFile>
#include <QByteArray>
#include <QDebug>
#include <QPixmap>

struct RGB
{
    char red;
    char green;
    char blue;
    RGB( char r,char g,char b )
    {
        red = r;
        green = g;
        blue = b;
    }
};

class BmpReader
{
private:
    QFile *file;
    QByteArray data;
    int width;
    int heigth;

public:
    BmpReader( QString file_name );
    RGB getPixel( int i );
    RGB getPixel( int i,int j );
    void setPixel( int i,RGB new_pixel );
    void setPixel( int i,int j,RGB new_pixel );
    void saveFile( QString file_name );

    int getWidth() { return width; }
    int getHeight(){ return heigth; }

    ~BmpReader();
};

#endif // BMPREADER_H
