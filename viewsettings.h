#ifndef VIEWSETTINGS_H
#define VIEWSETTINGS_H

#include <QDialog>

namespace Ui {
class ViewSettings;
}

class ViewSettings : public QDialog
{
    Q_OBJECT
private:
    int width_image;
    int heigth_image;
public:
    explicit ViewSettings(QWidget *parent = 0);
    bool getFlagRedComponent();
    bool getFlagGreenComponent();
    bool getFlagBlueComponent();

    bool isMethodInsertInfoOnFile();

    int getCountBits();
    int getCountPart();
    int getSizeBlock();
    int getMethod();

    void setSizeImage( int width,int heigth );

    ~ViewSettings();

private slots:
    void on_list_select_method_currentIndexChanged(int index);

    void on_txt_count_part_textChanged(const QString &arg1);

    void on_txt_size_block_textChanged(const QString &arg1);

    void on_radio_insert_info_message_clicked();

    void on_radio_insert_message_clicked();

private:
    Ui::ViewSettings *ui;
};

#endif // VIEWSETTINGS_H
