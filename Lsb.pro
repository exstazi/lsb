#-------------------------------------------------
#
# Project created by QtCreator 2015-05-22T09:45:14
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Lsb
TEMPLATE = app


SOURCES += main.cpp\
        viewmain.cpp \
    bmpreader.cpp \
    lsb.cpp \
    viewsettings.cpp \
    repeatedsequence.cpp \
    steganography.cpp \
    lsbrandom.cpp \
    lsbblock.cpp \
    lsbconsistent.cpp

HEADERS  += viewmain.h \
    bmpreader.h \
    lsb.h \
    viewsettings.h \
    repeatedsequence.h \
    keytranslator.h \
    steganography.h \
    lsbrandom.h \
    lsbblock.h \
    lsbconsistent.h

FORMS    += viewmain.ui \
    viewsettings.ui
