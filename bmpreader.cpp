#include "bmpreader.h"


/*
 * Считывание файла изображения
 * fle_name - имя файла
 */
BmpReader::BmpReader(QString file_name)
{
    file = new QFile( file_name );

    //Открываем файл на чтение
    file->open(QIODevice::ReadOnly);
    data.clear();
    //Считываем полностью весь файл
    data = file->readAll();
    //Проверяем файла на формат bmp
    if( !(data.at(0) == 0x42 || data.at(1) == 0x4d) )
    {
        qDebug() << "Формат не bmp";
    }
    //Считывание размеров изображения
    width =  (unsigned int)(data.at(21) << 24) + (unsigned int)(data.at(20) << 16) +
             (unsigned int)(data.at(19) << 8) + (unsigned char)data.at(18);

    heigth = (unsigned int)(data.at(25) << 24) + (unsigned int)(data.at(24) << 16) +
             (unsigned int)(data.at(23) << 8) + (unsigned char)data.at(22);

}

/*
 * Получение последовательного пиксела
 * i - номер пикселя
 */
RGB BmpReader::getPixel(int i)
{
    //Пикслеы находятся со смещением 54 байта от начала изображения
    //Каждый пиксел имеет размер 3 байта

    i = i*3 + 54;
    return RGB( data.at(i+2),data.at(i+1),data.at(i) );
}

/*
 * Получение пиксела в двухмерном массиве
 * i,j - координаты двухмерного массива
 */
RGB BmpReader::getPixel(int i, int j)
{
    long offset = 54 + 3 * ( ( j * width ) + i );
    return RGB( data.at(offset+2),data.at(offset+1),data.at(offset) );
}
/*
 * Замена последовательно пиксела
 * i - номер пикселя
 * new_pixel - заменяемый пиксель
 */
void BmpReader::setPixel(int i, RGB new_pixel)
{
    i *= 3;
    data[i+56] = new_pixel.red;
    data[i+55] = new_pixel.green;
    data[i+54] = new_pixel.blue;
}
/*
 * Замена пиксела в двухмерном массиве
 * i,j - координаты двухмерного массива
 * new_pixle - заменяемый пиксель
 */
void BmpReader::setPixel(int i, int j, RGB new_pixel)
{
    int offset = 54 + 3 * ( ( j * width ) + i );
    data[offset + 2] = new_pixel.red;
    data[offset + 1] = new_pixel.green;
    data[offset] = new_pixel.blue;
}

/*
 * Сохранение текущего изображения в файл
 * file_name - имя файла
 */
void BmpReader::saveFile(QString file_name)
{
    QFile file( file_name );

    if(!file.open(QIODevice::WriteOnly))
    {
        qDebug() << "file not open";
    }
    file.write( data );
}

BmpReader::~BmpReader()
{
    data.clear();
    file->close();
}

