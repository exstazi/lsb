#include "viewmain.h"
#include <QApplication>
#include "keytranslator.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    KeyTranslator *keyTranslator = new KeyTranslator();
    a.installEventFilter(keyTranslator);

    ViewMain w;
    w.show();

    return a.exec();
}
