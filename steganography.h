#ifndef STEGANOGRAPHY_H
#define STEGANOGRAPHY_H

#include <QBitArray>
#include <QByteArray>
#include <QVector>
#include <QPair>
#include "bmpreader.h"


class Steganography
{
protected:
    BmpReader *image;

    bool is_need_red_component;
    bool is_need_green_component;
    bool is_need_blue_component;
    int summa_component;

    int count_bits;

public:
    static QBitArray stringToBitArray( QString text );
    static QString bitArrayToString( QBitArray bit_array );

    virtual void writeMessage( QString message ) = 0;
    virtual void writeInfoOnFile( QString message ) = 0;

    virtual QString readMessage(int count) = 0;
    virtual QString readInfoOnFile( int count_block ) = 0;

    void setFlagsComponents( bool red,bool green,bool blue );
    void setCountBits( int count );

    int getCountBits() { return count_bits; }

    virtual int getMaxLengthMessage() = 0;

    void deleteBits();

protected:
    void writeToFile(QVector<QPair<int,int> > position, QBitArray message );
    QString readFile(QVector<QPair<int,int> > position , int count);

    int readCountSymbolsForInfoOnFile(int number_block);
    void writeCountSymbolsForInfoOnFile( int length );
};

#endif // STEGANOGRAPHY_H
