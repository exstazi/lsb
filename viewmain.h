#ifndef VIEWMAIN_H
#define VIEWMAIN_H

#include <QMainWindow>
#include <QResizeEvent>
#include <QTime>
#include "bmpreader.h"
#include "lsb.h"
#include "viewsettings.h"

#include "steganography.h"
#include "lsbrandom.h"
#include "lsbconsistent.h"

namespace Ui {
class ViewMain;
}

class ViewMain : public QMainWindow
{
    Q_OBJECT

public:
    explicit ViewMain(QWidget *parent = 0);
    ~ViewMain();

private:
    Ui::ViewMain *ui;
    BmpReader *reader;
    Steganography *lsb;
    ViewSettings *settings;
    QPixmap *image_preview;
    void previewImage();
    QPixmap *drawGrid();
    QTime timer;


    void setEnabledUi( bool value );
    void setInfoOnImage(QString name, QString size_image, int size_file, QString max_length_message);
    QString getFileName();

protected slots:
    void openFile();
    void openSettings();
    void openDeleteBits();
    void closeFile();
    void resizeEvent(QResizeEvent *event);
private slots:
    void on_btn_insert_message_clicked();
    void on_btn_save_file_clicked();
    void on_btn_remove_clicked();
    void on_txt_insert_message_textChanged();
};

#endif // VIEWMAIN_H
