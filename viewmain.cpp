#include "viewmain.h"
#include "ui_viewmain.h"
#include <QPixmap>
#include <QBitmap>
#include <QDebug>
#include <QBitArray>
#include <QByteArray>
#include <QFile>
#include <QFileDialog>
#include "lsb.h"
#include "viewsettings.h"
#include <QMessageBox>
#include <QPainter>

ViewMain::ViewMain(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ViewMain)
{
    ui->setupUi(this);

    QAction *menu_settins = ui->menu_bar->addAction("Настройки");
    QMenu *menu_service = new QMenu(tr("Дополнительно"));
    QAction *menu_delete_bit = new QAction(tr("Удаление бит"),this);
    menu_service->addAction(menu_delete_bit);
    ui->menu_bar->addMenu( menu_service);

    connect( ui->menu_open_file,SIGNAL(triggered()),this,SLOT(openFile()) );
    connect( ui->menu_close_app,SIGNAL(triggered()),this,SLOT(closeFile()) );
    connect( menu_settins,SIGNAL(triggered()),this,SLOT(openSettings()) );
    connect( menu_delete_bit,SIGNAL(triggered()),this,SLOT(openDeleteBits()) );

    settings = new ViewSettings( this );
    settings->setModal( true );
    settings->setWindowFlags( settings->windowFlags() & ~Qt::WindowContextHelpButtonHint );

    image_preview = NULL;
    reader = NULL;
    lsb = NULL;

    setEnabledUi( false );
}

void ViewMain::setEnabledUi(bool value)
{
    ui->tabWidget->setEnabled( value );
    ui->menu_bar->actions().at(1)->setEnabled( value );
    ui->menu_bar->actions().at(2)->setEnabled( value );
}

void ViewMain::setInfoOnImage(QString name, QString size_image, int size_file, QString max_length_message)
{
    ui->txt_name_file->setText(name);
    ui->txt_size_image->setText(size_image);
    ui->txt_size_file->setText(QString::number(size_file) + " px");
    ui->txt_max_message_size->setText(max_length_message + " симв.");

    ui->lbl_count_symbols->setText(max_length_message + " симв.");
    ui->txt_count_symbols->setText(max_length_message);
}

void ViewMain::openFile()
{
    QString file_name = QFileDialog::getOpenFileName(this, tr("Open File"),"",tr("*.bmp"));

    if( file_name.size() < 5 )
    {
        ui->txt_status_bar->setText( "Файл не найден." );
        return ;
    }

    reader = new BmpReader( file_name );
    lsb = new LSB( reader );
    image_preview = new QPixmap( file_name );

    setInfoOnImage( file_name.right(file_name.size() - file_name.lastIndexOf('/')-1),
                    QString::number( image_preview->width() ) + " x " + QString::number( image_preview->height() ),
                    image_preview->width() * image_preview->height(),
                    QString::number( lsb->getMaxLengthMessage() ) );

    setEnabledUi( true );
    ui->btn_save_file->setEnabled( false );

    ui->txt_status_bar->setText( "Изображение загружено." );

    previewImage();
}

void ViewMain::closeFile()
{
    setEnabledUi(false);
    ui->txt_count_symbols->setText( "" );
    ui->txt_insert_message->setPlainText( "" );
    ui->txt_max_message_size->setText( "-" );
    ui->txt_name_file->setText( "-" );
    ui->txt_remove_message->setPlainText( "" );
    ui->txt_size_file->setText( "-" );
    ui->txt_size_image->setText( "-" );
    ui->txt_status_bar->setText( "-" );
    ui->lbl_count_symbols->setText( "xxxx" );

    ui->txt_time_insert->setText( "" );
    ui->txt_time_remote->setText( "" );

    ui->picture->clear();
}

QPixmap* ViewMain::drawGrid()
{
    QPixmap *img = new QPixmap( *image_preview );
    QPainter p(img);

    qreal w = (qreal)reader->getWidth() / settings->getCountPart();
    qreal h = (qreal)reader->getHeight()/ settings->getCountPart();
    int count_part = settings->getCountPart();

    if( settings->isMethodInsertInfoOnFile() )
    {
        w /= 2;
        h /= 2;
        count_part *= 2;
    }

    for( int i = 1; i < count_part; ++i )
    {
        p.drawLine( QLineF( w * i, 0, w * i, reader->getHeight() ) );
        p.drawLine( QLineF( 0, h * i, reader->getWidth(), h * i ) );
    }

    if( settings->isMethodInsertInfoOnFile() )
    {
        p.setPen( Qt::green );
        p.drawLine( QLineF( 0, reader->getHeight()/2, reader->getWidth() , reader->getHeight()/2 ) );
        p.drawLine( QLineF( reader->getWidth() / 2, 0, reader->getWidth() / 2, reader->getHeight() ) );
    }

    return img;
}

void ViewMain::previewImage()
{
    QPixmap *preview;
    if( settings->getMethod() == 0 )
    {
        preview = image_preview;
    } else {
        preview = drawGrid();
    }

    ui->picture->setPixmap( preview->scaled( ui->picture->geometry().width(),
                                             ui->picture->geometry().height(),
                                             Qt::KeepAspectRatio,Qt::SmoothTransformation ) );
}

void ViewMain::openSettings()
{
    settings->setSizeImage( reader->getWidth(),reader->getHeight() );

    if( settings->exec() )
    {
        delete lsb;
        switch( settings->getMethod() )
        {
        case 0:
            lsb = new LSB( reader );
            break;
        case 1:
            lsb = new LsbRandom( reader,settings->getCountPart(),settings->getSizeBlock() );
            break;
        case 2:
            lsb = new LsbConsistent( reader,settings->getCountPart(),settings->getSizeBlock() );
            break;
        }

        QString status;
        lsb->setFlagsComponents( settings->getFlagRedComponent(),
                                 settings->getFlagGreenComponent(),
                                 settings->getFlagBlueComponent() );
        lsb->setCountBits( settings->getCountBits() );

        status.append( "Выбрано: Метод " +
                       (settings->getMethod()==0?"LSB":QString::number(settings->getMethod()+1)) +
                       "; Кол-во бит: " + QString::number( settings->getCountBits()) +
                       "; Red: " + (settings->getFlagRedComponent()?"yes":"no") +
                       "; Green: " + (settings->getFlagGreenComponent()?"yes":"no") +
                       "; Blue: " + (settings->getFlagBlueComponent()?"yes":"no") );

        if( settings->getMethod() > 0 )
        {
            status.append( "; Кол-во частей: " + QString::number( settings->getCountPart() ) +
                           "; Размер блока: " + QString::number( settings->getSizeBlock() ) );
        }
        ui->txt_status_bar->setText( status );
        ui->txt_count_symbols->setText( QString::number( lsb->getMaxLengthMessage() ) );
        ui->txt_max_message_size->setText( ui->txt_count_symbols->text() + " симв." );
        ui->lbl_count_symbols->setText( ui->txt_max_message_size->text() );
        status.clear();

        if( settings->isMethodInsertInfoOnFile() )
        {
            ui->txt_insert_message->setPlainText( ui->txt_name_file->text() + ";" + ui->txt_size_image->text() + ";" +
                                                  ui->txt_status_bar->text() +";" );
            ui->txt_insert_message->setEnabled( false );
            ui->txt_count_symbols->setText( "1" );
            ui->lbl_input_text->setText( "Введите номер блока" );
        } else {
            ui->txt_insert_message->setEnabled( true );
            ui->lbl_input_text->setText( "Количество символов" );
        }
        previewImage();
        on_txt_insert_message_textChanged();
    }
}

void ViewMain::openDeleteBits()
{
    QString file = getFileName();
    if( file == "" )
        return ;

    BmpReader *file_bits = new BmpReader( *reader );

    char mask;
    for( int i = 0; i < (8-lsb->getCountBits()); ++i )
    {
        mask |= ( 1 << (7-i) );
    }
    for( int i = 0; i < file_bits->getWidth(); ++i )
    {
        for( int j = 0; j < file_bits->getHeight(); ++j )
        {
            RGB color = file_bits->getPixel( i,j );

            color.red = (color.red & 0x01)?mask:(color.red & mask);
            color.green = (color.green & 0x01)?mask:(color.green & mask);
            color.blue = (color.blue & 0x01)?mask:(color.blue & mask);

            file_bits->setPixel( i,j,color );
        }
    }

    file_bits->saveFile( file );
    ui->txt_status_bar->setText( "Изображение сохранено в " + file );

}

QString ViewMain::getFileName()
{
    QString file_name = QFileDialog::getSaveFileName( this,tr("Save File"),"",tr("*bmp") );
    if( file_name == "" )
    {
        return "";
    }

    if( file_name.size() > 4 )
    {
        if( file_name.right(4) != ".bmp" )
        {
            file_name.append( ".bmp" );
        }
    }
    else
    {
        file_name.append( ".bmp" );
    }
    return file_name;
}

void ViewMain::on_btn_insert_message_clicked()
{
    if( settings->isMethodInsertInfoOnFile()  )
    {
        lsb->writeInfoOnFile( ui->txt_insert_message->toPlainText() );
        ui->btn_save_file->setEnabled( true );
        ui->txt_status_bar->setText( "В изображение встроена информация о файле " );
    }
    else if( ui->txt_insert_message->toPlainText().size() != 0 )
    {
        timer.start();
        lsb->writeMessage( ui->txt_insert_message->toPlainText() );
        ui->txt_time_insert->setText( "Встраивание заняло: " + QString::number( timer.elapsed() ) + " мсек." );
        ui->btn_save_file->setEnabled( true );
        ui->txt_status_bar->setText( "В изображение встроено " +
                                     QString::number( ui->txt_insert_message->toPlainText().size() ) + " симв." );
    }
}

void ViewMain::on_btn_save_file_clicked()
{
    QString file = getFileName();
    if( file == "" )
        return ;
    reader->saveFile( file );
    ui->txt_status_bar->setText( "Изображение сохранено в " + file );
}

void ViewMain::on_btn_remove_clicked()
{
    if( settings->isMethodInsertInfoOnFile() )
    {
        int count_symbols = ui->txt_count_symbols->text().toInt() - 1;
        if( count_symbols > 4 || count_symbols < 0 )
        {
            QMessageBox::information(this,QString::fromUtf8("Предупреждение"),
                                   QString::fromUtf8("Номер блока лежит в диапозоне [1..4]"));
        } else {            
            ui->txt_remove_message->clear();
            ui->txt_remove_message->appendPlainText( lsb->readInfoOnFile( count_symbols ) );
            ui->txt_status_bar->setText( "Прочитан " + ui->txt_count_symbols->text() + " блок."  );
        }
    }
    else if( ui->txt_count_symbols->text().size() != 0 )
    {
        ui->txt_remove_message->clear();
        timer.start();
        ui->txt_remove_message->appendPlainText( lsb->readMessage( ui->txt_count_symbols->text().toInt() ) );
        ui->txt_time_remote->setText( "Извлечение заняло: " + QString::number( timer.elapsed() ) + " мсек.");
        ui->txt_status_bar->setText( "Прочитано " + ui->txt_count_symbols->text() + " симв."  );
    }
}

void ViewMain::on_txt_insert_message_textChanged()
{
    int count = lsb->getMaxLengthMessage() - ui->txt_insert_message->toPlainText().size();
    if( count < 0 )
    {
        QMessageBox::information( this, QString::fromUtf8("Предупреждение"),
                     QString::fromUtf8("Превышено максимально возможное количество символов"));
        ui->txt_insert_message->setPlainText( ui->txt_insert_message->toPlainText().left( lsb->getMaxLengthMessage() ) );
    }
    else
    {
        ui->lbl_count_symbols->setText( QString::number( count ) + " симв." );
    }
}

void ViewMain::resizeEvent(QResizeEvent *event)
{
    ui->tabWidget->setGeometry(10,20,321,event->size().height() - 200);
    ui->picture->setGeometry( 0,0,event->size().width() - 350, event->size().height() - 29 );
    if( image_preview != NULL )
    {
        previewImage();
    }
}

ViewMain::~ViewMain()
{
    delete ui;
}
