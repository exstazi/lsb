#include "lsb.h"
#include <QBitArray>
#include <QSet>

LSB::LSB(BmpReader *reader)
{
    image = reader;

    count_bits = 1;
    is_need_red_component =
            is_need_green_component =
                    is_need_blue_component = true;
    summa_component = 3;
}

/*
 * Считывание встроенного сообщения
 * count - количество символов, которые будут прочитаны
 */
QString LSB::readMessage(int count)
{
    //Переводим в количество бит
    count *= 8;
    //Рассчет необходимого количества пикселей
    int bit_in_pixel = count_bits * summa_component;
    int count_pixel = count / bit_in_pixel + ( (count % bit_in_pixel)==0?0:1);

    QVector<QPair<int, int> > positon;

    for( int i = 0; i < count_pixel; ++i )
    {
        //рассчитываем координаты следующего пикселя
        positon.append( QPair<int, int>( i % image->getWidth(), i/ image->getHeight() ) );
    }

    return readFile( positon,count / 8 );
}

/*
 * Встраивание сообщения в файл
 * message - встраиваемое сообщение
 */
void LSB::writeMessage(QString message)
{
    QBitArray arr( stringToBitArray( message ) );
    //Рассчет необходимого количества пикселей
    int bit_in_pixel = count_bits * summa_component;
    int count_pixel = (message.size() * 8) / bit_in_pixel + ( ((message.size() * 8) % bit_in_pixel)==0?0:1);

    QVector<QPair<int, int> > positon;

    for( int i = 0; i < count_pixel; ++i )
    {
        //рассчитываем координаты следующего пикселя
        positon.append( QPair<int, int>( i % image->getWidth(), i/ image->getHeight() ) );
    }
    writeToFile( positon,arr );
}

/*
 * Считывание информации о файле в выбранном блоке
 * block - номер блока, откуда происходит считывание
 */
QString LSB::readInfoOnFile( int block )
{
    //Считываем длину встроенного сообщения
    int length_info = readCountSymbolsForInfoOnFile( block );
    //Вычислеям размеры одного блока
    int w_border = image->getWidth() / 2;
    int h_border = ( image->getHeight() / 2) - 1;
    //Вычисляем необходимое количество блоко
    int count_pixel = ( length_info * 8 ) / (summa_component * count_bits) + ((length_info * 8)%(summa_component*count_bits)!=0?1:0);

    QVector<QPair<int,int> > position;

    for( int i = 0; i < count_pixel; ++i )
    {
        //рассчитываем координаты следующего пикселя
        position.append( QPair<int,int>( i%w_border  + block/2 *w_border,(i/h_border) + 1 + block%2 *h_border ) );
    }
    return readFile( position,length_info );
}

/*
 * Запись информации о файле
 * message - инофрмация о файле
 */
void LSB::writeInfoOnFile( QString message )
{
    //Добавление информаци о номере блока
    message.append( "Block 0" );
    //Если длина сообщения превышает максимальную длину, то обрезаем сообщениеы
    if( message.size() > getMaxLengthMessage() )
    {
        message.resize( getMaxLengthMessage() );
    }
    //Получаем размеры одного блока
    int w_border = image->getWidth() / 2;
    int h_border = (image->getHeight() / 2) - 1;
    //Встраиваем в первые 2 пикселя каждого блока длину сообщения
    writeCountSymbolsForInfoOnFile( message.size() );
    //Преобразовываем сообщение в массив бит
    QBitArray arr( stringToBitArray(message) );

    QVector<QPair<int,int> > position;

    //Рассчет количество необходимых пикселей
    int count_pixel = arr.size() / (summa_component * count_bits) + (arr.size()%(summa_component*count_bits)!=0?1:0);

    for( int n = 0; n < 4; ++n )
    {
        //Заменяем номер блока в сообщения на текущий счетчик
        arr = stringToBitArray( message.left( message.size()-1) + QString::number(n+1) );
        for( int i = 0; i < count_pixel; ++i )
        {
            //рассчитываем координаты следующего пикселя
            position.append( QPair<int,int>( i%w_border + n/2 *w_border,(i/h_border) + 1 + n%2 *h_border ) );
        }
        //Записываем в файл
        writeToFile( position, arr );
        position.clear();
    }
}

/*
 * Рассчтем максимальной длины встраиваемого сообщения
 */
int LSB::getMaxLengthMessage()
{
    int length = ( image->getWidth() * image->getHeight() * summa_component * count_bits );
    length = length / 8 + (length%8==0?0:1);
    return  length;
}


