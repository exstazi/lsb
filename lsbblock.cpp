#include "lsbblock.h"

/*
 * Получение координат пикслей в выбранном блоке
 * number_block - блок, из которого получаем пикселы
 * w_part - ширина блока
 * h_part - высота блока
 * start_offset_w - смещение блока относитально начала изображения по ширине
 * start-offset_h - смещение блока относитально начала изображения по высоте
 */
QVector<QPair<int,int> > LsbBlock::getCoordinate(int number_block,int w_part,int h_part,int start_offset_w,int start_offset_h)
{
    QVector<QPair<int,int> > list;

    //Рассчет смещения относительно начала изображения
    int offset_w = ((w_part - size_block) / 2) + ( number_block % count_part ) * w_part + start_offset_w;
    int offset_h = ((h_part - size_block) / 2) + ( number_block / count_part ) * h_part + start_offset_h;
    //Проход по квадрату блока и вычисление его координат
    for( int i = 0; i < size_block; ++i )
    {
        for( int j = 0; j < size_block; ++j )
        {
            list.append( QPair<int,int>( offset_w + j, offset_h + i ) );
        }
    }
    return list;
}

/*
 * Установка количества блоков, на которые разбивется изображение
 * value - колиечство блоков
 */
void LsbBlock::setCountPart(int value)
{
    if( value > 0 )
    {
        count_part = value;
    }
}
/*
 * Установка размеров блока
 * value - размер блока
 */
void LsbBlock::setSizeBlock(int value)
{
    if( value > 0 )
    {
        size_block = value;
    }
}

/*
 * Вычисление необходимого количества блоков, для встраивания count битов
 * count - колиечстов битов для встривания
 */
int LsbBlock::getDesiredCountBlock(int count)
{
    int bit_in_block = size_block * size_block * count_bits * summa_component;
    return ( (count / bit_in_block )+ ((count % bit_in_block)!=0?1:0) );
}
/*
 * Рассчет максимальной длины сообщения
 */
int LsbBlock::getMaxLengthMessage()
{
    int length = ( count_part*count_part * size_block * size_block * summa_component * count_bits );
    length = length / 8 + (length%8==0?0:1);
    return length;
}
