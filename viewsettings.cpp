#include "viewsettings.h"
#include "ui_viewsettings.h"
#include <QDebug>
#include <QRegExp>
#include <QRegExpValidator>

ViewSettings::ViewSettings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ViewSettings)
{
    ui->setupUi(this);

    ui->txt_count_part->setVisible( false );
    ui->txt_size_block->setVisible( false );
    ui->lbl_count_part->setVisible( false );
    ui->lbl_size_block->setVisible( false );
    ui->lbl_max_count_part->setVisible( false );
    ui->lbl_max_size_block->setVisible( false );

    ui->txt_count_part->setValidator( new QRegExpValidator(QRegExp( "^[1-9][0-9]*" ) ) );
    ui->txt_size_block->setValidator( new QRegExpValidator(QRegExp( "^[1-9][0-9]*" ) ) );

    ui->txt_size_block->setEnabled( false );
}


bool ViewSettings::getFlagRedComponent()
{
    return ui->check_red_component->isChecked();
}

bool ViewSettings::getFlagGreenComponent()
{
    return ui->check_green_component->isChecked();
}

bool ViewSettings::getFlagBlueComponent()
{
    return ui->check_blue_component->isChecked();
}

bool ViewSettings::isMethodInsertInfoOnFile()
{
    return ui->radio_insert_info_message->isChecked();
}

int ViewSettings::getCountBits()
{
    return ui->list_count_bits->currentIndex()+1;
}

int ViewSettings::getCountPart()
{
    return ui->txt_count_part->text().toInt();
}

int ViewSettings::getSizeBlock()
{
    return ui->txt_size_block->text().toInt();
}

int ViewSettings::getMethod()
{
    return ui->list_select_method->currentIndex();
}

void ViewSettings::on_list_select_method_currentIndexChanged(int index)
{
    if( index == 0 )
    {
        ui->txt_count_part->setVisible( false );
        ui->txt_size_block->setVisible( false );
        ui->lbl_count_part->setVisible( false );
        ui->lbl_size_block->setVisible( false );
        ui->lbl_max_count_part->setVisible( false );
        ui->lbl_max_size_block->setVisible( false );
    }
    else
    {
        ui->txt_count_part->setVisible( true );
        ui->txt_size_block->setVisible( true );
        ui->lbl_count_part->setVisible( true );
        ui->lbl_size_block->setVisible( true );
        ui->lbl_max_count_part->setVisible( true );
        ui->lbl_max_size_block->setVisible( true );
    }
}

void ViewSettings::setSizeImage(int width, int heigth)
{
    width_image = width;
    heigth_image = heigth;

    ui->lbl_max_count_part->setText( "Макс.:" + (width_image>heigth_image?
                                         QString::number( heigth_image ):
                                         QString::number( width_image )) );
}

void ViewSettings::on_txt_count_part_textChanged(const QString &arg1)
{
    if( arg1.size() != 0 )
    {
        int count = arg1.toInt();
        int w = width_image / count + (width_image % count!=0?1:0),
            h = heigth_image / count + (heigth_image %count!=0?1:0);
        if( ( ( count > width_image || count > heigth_image ) && ui->radio_insert_message->isChecked() ) ||
               ( ( count > width_image/2 || count > heigth_image/2 ) && ui->radio_insert_info_message->isChecked() ) )
        {
            QString text = ui->txt_count_part->text();
            text.resize( text.size() - 1 );
            ui->txt_count_part->setText( text );
        }
        else
        {
            if( ui->radio_insert_message->isChecked() )
                ui->lbl_max_size_block->setText( "Макс.:" + (w>h?QString::number(h):QString::number(w)));
            else
               ui->lbl_max_size_block->setText( "Макс.:" + (w>h?QString::number(h/2):QString::number(w/2)));
            ui->txt_size_block->setEnabled( true );
        }
    } else {
        ui->lbl_max_size_block->setText( "Макс.:" );
        ui->txt_size_block->setEnabled( false );
        ui->txt_size_block->setText( "" );
    }

}

void ViewSettings::on_txt_size_block_textChanged(const QString &arg1)
{
    if( arg1.size() != 0  )
    {
        int count = ui->txt_count_part->text().toInt();
        int w = width_image / count,
            h = heigth_image / count;
        if( arg1.toInt() > w || arg1.toInt() > h )
        {
            QString text = ui->txt_size_block->text();
            text.resize( text.size() - 1 );
            ui->txt_size_block->setText( text );
        }
    }
}

void ViewSettings::on_radio_insert_info_message_clicked()
{
    ui->lbl_max_count_part->setText( "Макс.:" + (width_image>heigth_image?
                                         QString::number( heigth_image/2 ):
                                         QString::number( width_image/2 )) );
    ui->txt_count_part->setText( "");
    ui->txt_size_block->setText( "" );
    ui->txt_size_block->setEnabled(false);
}

void ViewSettings::on_radio_insert_message_clicked()
{
    ui->lbl_max_count_part->setText( "Макс.:" + (width_image>heigth_image?
                                         QString::number( heigth_image ):
                                         QString::number( width_image )) );
}

ViewSettings::~ViewSettings()
{
    delete ui;
}
