#include "repeatedsequence.h"
#include <QDebug>

int RepeatedSequence::A = 640;
int RepeatedSequence::B = 480;
int RepeatedSequence::M = 640;
long RepeatedSequence::seed = 2;
QList<int> RepeatedSequence::set = QList<int>();

/*
 * Генерация следующего псевдослучайного числа
 * A - ширина изображения
 * B - высота изображения
 * M - максимальное количество блоков
 * seed - количество символов встраемого сообщения
 */
int RepeatedSequence::rand()
{
    seed = ( A * seed + B ) % M;
    while( set.indexOf( seed ) != -1 )
    {
        seed = ( seed + 1 ) % M;
    }
    set.append( seed );
    return seed;
}

void RepeatedSequence::setSeed(int s)
{
    seed = s;
    set.clear();
}

void RepeatedSequence::setParam(int a, int b,int m )
{
    A = a;
    B = b;
    M = m;
}
