#ifndef LSB_H
#define LSB_H

#include "steganography.h"

class LSB : public Steganography
{

public:
    LSB( BmpReader *reader );
    QString readMessage( int count );
    void writeMessage( QString message );

    void writeInfoOnFile(QString message);
    QString readInfoOnFile(int block );

    int getMaxLengthMessage();
};

#endif
