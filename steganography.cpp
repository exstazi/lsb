#include "steganography.h"


/*
 * Встраивание в изображение переданного сообщения
 * position – массив координат пикселей, в которые необходимо встроить изображение
 * message – встраиваемое изображение в  виде массива бит
 */
void Steganography::writeToFile(QVector<QPair<int, int> > position, QBitArray message)
{
    for( int i = 0,k = 0; i < position.size(); ++i )
    {
        //Получем цвета выбранного пикселя
        RGB color = image->getPixel( position.at(i).first,position.at(i).second );
        //Перебираем необходимое количество бит
        for( int j = 0; j < count_bits; ++j )
        {
            //Если данная составляющая учавствует в встривании и сообщение еще не полностью встроено
            if( is_need_blue_component && k < message.size() )
            {
                //Записываем в j бит цвета k бит сообщения
                color.blue = (color.blue & ~(1<<j)) | ( message.at(k++) << j);
            }
            if( is_need_green_component && k < message.size() )
            {
                color.green = (color.green & ~(1<<j)) | ( message.at(k++) << j);
            }
            if( is_need_red_component && k < message.size() )
            {
                color.red = (color.red & ~(1<<j)) | ( message.at(k++) << j);
            }
        }
        //Записываем измененных пикселей
        image->setPixel( position.at(i).first,position.at(i).second,color );
    }
    //Очищаем переданные координаты и сообщения
    position.clear();
    message.clear();
}
/*
 * Считывание из файла встроенной информации
 * position – массив координат пикселей, из который необходимо извлечь информацию
 * count - количество символов, которые необходимо извлечь
 */
QString Steganography::readFile(QVector<QPair<int, int> > position, int count )
{
    //Создаем массив битов, равный размеру сообщения
    QBitArray arr(count*8);

    for( int i = 0,k = 0; i < position.size(); ++i )
    {
        //Получаем цвета выбранного пискеля
        RGB color = image->getPixel( position.at(i).first,position.at(i).second );
        //Перебираем необходимое количество бит
        for( int j = 0; j < count_bits; ++j )
        {
            //Если данная составляющая учавствует в встривании и сообщение еще не полностью прочитано
            if( is_need_blue_component && k < arr.size() )
            {
                //Устанавливаем k бит сообщения в значение j бита цвета
                arr.setBit(k++,(color.blue >> j ) & 0x01);
            }
            if( is_need_green_component && k < arr.size() )
            {
                arr.setBit(k++,(color.green >> j ) & 0x01);
            }
            if( is_need_red_component && k < arr.size() )
            {
                arr.setBit(k++,(color.red >> j ) & 0x01);
            }
        }
    }
    //Преобразуем массив бит в строку
    return bitArrayToString( arr );
}

/*
 * Установка флагов компонент
 * red, green, blue - учавствует ли данная компонента при встраивании
 */
void Steganography::setFlagsComponents(bool red, bool green, bool blue)
{
    is_need_red_component = red;
    is_need_green_component = green;
    is_need_blue_component = blue;
    summa_component = is_need_red_component +
                        is_need_green_component +
                            is_need_blue_component;
}

/*
 * Установка количества бит для встривания изображения
 * count - количество бит
 */
void Steganography::setCountBits(int count)
{
    if( count > 0 && count < 9 )
    {
        count_bits = count;
    }
}

/*
 * Преобразование строки в массив бит
 * text - входная строка для преобразования
 */
QBitArray Steganography::stringToBitArray(QString text)
{
    QBitArray array(text.size() * 8 );
    QByteArray char_text = text.toLocal8Bit();

    for( int i = 0; i < text.size(); ++i )
    {
        for( int j = 0; j < 8; ++j )
        {
            array.setBit( i*8+j, char_text.at(i) & (1<<j) );
        }
    }

    return array;
}

/*
 * Преобразование массива бит в строку
 * bit_array - входной массив для преобразования
 */
QString Steganography::bitArrayToString(QBitArray bit_array)
{
    QByteArray bytes;
    bytes.resize(bit_array.size()/8+1);
    bytes.fill(0);

    for( int b = 0; b < bit_array.size(); ++b )
    {
        bytes[b/8] = ( bytes.at(b/8) | ((bit_array[b]?1:0)<<(b%8)) );
    }

    return QString::fromLocal8Bit( bytes );
}

/*
 * Считывание из первых 2 пикселей, информации о длине встроенной информации о файле
 * В каждый цвет встроено 3 бита информации, в результате получаем 18 бит, что
 * соответсвует длине сообщения в  262143 символа.
 * number_block - номер блока из которого производится считывание. [1..4]
 */
int Steganography::readCountSymbolsForInfoOnFile( int number_block )
{
    //Размеры одного блока
    int w_border = image->getWidth() / 2;
    int h_border = (image->getHeight() / 2) - 1;

    int length_info = 0;
    //Забираем 2 пиксель из выбранного блока
    RGB color = image->getPixel( number_block/2 * w_border,number_block%2 * h_border + 1 );
    //Забираем от каддой состовляющей цвета первые 3 бита
    length_info = (int)((color.blue & 0x07 ) << 6) | (int)((color.green & 0x07) << 3) | (int)(color.red & 0x07);
    length_info <<= 9;
    //Забираем 1 пиксель из выбранного блока
    color = image->getPixel( number_block/2 * w_border,number_block%2 * h_border );
    length_info = (int)((color.blue & 0x07 ) << 6) | (int)((color.green & 0x07) << 3) | (int)(color.red & 0x07);

    return length_info;
}

/*
 * Запись в первые 2 пикселя каждого блока информации о длине встраиваемого сообщения
 * length - длина сообщения
 */
void Steganography::writeCountSymbolsForInfoOnFile(int length)
{
    //Размеры одного блока
    int w_border = image->getWidth() / 2;
    int h_border = (image->getHeight() / 2) - 1;

    int length_info = 0;

    //Перебираем все блоки
    for( int i = 0; i < 4; ++i )
    {
        length_info = length;
        //Получем 1 пиксель i блока
        RGB color = image->getPixel( i/2 * w_border,i%2 * h_border );
        //Записывые по 3 бита в каждую составляющую
        color.red = (color.red & 0xF8) | ( length_info & 0x07 );
        color.green = (color.green & 0xF8 ) | ( ( length_info >>= 3 ) & 0x07 );
        color.blue = (color.blue & 0xF8 ) | ( ( length_info >>= 3 ) & 0x07 );
        //Записываем измененный пиксель
        image->setPixel(i/2 * w_border,i%2 * h_border,color);
        color = image->getPixel(i/2 * w_border,i%2 * h_border + 1);
        color.red = (color.red & 0xF8 ) | ( ( length_info >>= 3 ) & 0x07 );
        color.green = (color.green & 0xF8 ) | ( ( length_info >>= 3 ) & 0x07 );
        color.blue = (color.blue & 0xF8 ) | ( ( length_info >>= 3 ) & 0x07 );
        image->setPixel(i/2 * w_border,i%2 * h_border + 1,color);
    }
}

